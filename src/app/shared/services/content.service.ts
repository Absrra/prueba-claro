import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  private title: BehaviorSubject<string> = new BehaviorSubject(
    'Este es el titulo'
  );
  private body: BehaviorSubject<string> = new BehaviorSubject(
    'Este es el contenido principal de la pagina'
  );
  private footer: BehaviorSubject<string> = new BehaviorSubject(
    'Este es el footer '
  );

  constructor() { }

  getTitle() {
    return this.title.asObservable();
  }

  getBody() {
    return this.body.asObservable();
  }

  getFooter() {
    return this.footer.asObservable();
  }

  changeTitle(v: string) {
    this.title.next(v);
  }

}
