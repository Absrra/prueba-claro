import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ContentService } from './shared/services/content.service';
import { GetContentService } from './shared/services/get-content.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  
  public title?: string;
  public body?: string;
  public footer?: string;

  private subcriptions?: Subscription;

  constructor(
    private readonly contentService: ContentService
  ) {}


  ngOnInit(): void {
    this.getData();
    // this.getContent();
  }

  private getData() {
    this.contentService.getTitle().subscribe((title: string)  =>  {
      console.log('title', title)
      this.title  = title;
    })

    this.contentService.getBody().subscribe((body: string)  =>  {
      this.body = body;
    })

    this.contentService.getFooter().subscribe((footer: string)  =>  {
      this.footer = footer;
    })
  }

  ngOnDestroy(): void {
    // this.subcriptions?.unsubscribe();
  }

}
