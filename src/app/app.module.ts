import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChangeContentComponent } from './change-content/change-content.component';
import { ContentService } from './shared/services/content.service';
import { GetContentService } from './shared/services/get-content.service';

@NgModule({
  declarations: [
    AppComponent,
    ChangeContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    GetContentService,
    ContentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
