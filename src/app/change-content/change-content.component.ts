import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ContentService } from '../shared/services/content.service';
import { GetContentService } from '../shared/services/get-content.service';


@Component({
  selector: 'change-content',
  templateUrl: './change-content.component.html',
  styleUrls: ['./change-content.component.scss'],
})
export class ChangeContentComponent implements OnInit {

  newTitle: string = 'Este es el titulo modificado por otro componente';
  hasError = false;
  hasContent = false;

  constructor(
    private service: ContentService,
    private readonly getContentService: GetContentService,
  ) { }

  changeTitle() {
    this.service.changeTitle(this.newTitle);
  }

  callApi() {
    this.hasError = this.hasContent = false;
    this.getContentService.getContent().subscribe((response) => {
      this.hasContent = response ? true : false;
    }, (error: any) =>  {
      console.log('error', error);
      this.hasError = error ? true : false;
    });

  }


  ngOnInit(): void {
  }

}
